package ru.yaleksandrova.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProject();

    void createProject();

}
