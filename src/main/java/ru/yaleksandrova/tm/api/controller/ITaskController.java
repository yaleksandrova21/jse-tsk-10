package ru.yaleksandrova.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTask();

    void createTask();

}
