package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.model.Task;
import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

}
