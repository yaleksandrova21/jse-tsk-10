package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.ITaskRepository;
import ru.yaleksandrova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

}
